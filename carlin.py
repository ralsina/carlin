"""
HackWM
A hackable, modular window manager
by Dylan Hamer
"""

# Xlib imports - Interface with the X window system
from Xlib import X
from Xlib.display import Display

# HackWM imports - Core functions of HackWM
from keyboard import Keyboard
from mouse import Mouse
from mapping import Mapping
from wallpaper import set_wallpaper
from layout import TwoColumnLayout


class Wm:
    def __init__(self):
        self.display = Display()  # Initialise display
        self.root_window = self.display.screen().root  # Initialise root window
        self.layout = TwoColumnLayout(self.root_window)
        self.mouse_handler = Mouse(self.layout)
        self.root_window.change_attributes(event_mask=X.SubstructureRedirectMask)

        self.keyboard_handler = Keyboard(self.display, self.root_window)
        self.mapping_handler = Mapping(self.display, self.layout, self.mouse_handler)

        set_wallpaper()

    def handle_events(self):
        # self.display.pending_events() > 0:  # If there is an event in the queue
        if True:
            event = self.display.next_event()  # Grab it
            print("Got an event! ({})".format(str(event.type)))
            if event.type == X.KeyPress:
                self.keyboard_handler.handle_key_event(event)
            elif event.type == X.MapRequest:
                self.mapping_handler.handle_map_event(event)
            elif event.type == X.UnmapNotify:
                self.mapping_handler.handle_unmap_event(event)
            elif event.type == X.ButtonPress:
                self.mouse_handler.handle_mouse_event(event)
            elif event.type == X.ButtonRelease:
                self.mouse_handler.handle_mouse_event(event)
            elif event.type == X.MotionNotify:
                self.mouse_handler.handle_mouse_event(event)

    def loop(self):
        while True:
            self.handle_events()
            self.mapping_handler.draw_borders()


Wm().loop()
