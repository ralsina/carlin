from Xlib import X


class WindowColumn:
    """A sub-manager that puts windows in a column."""

    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.windows = []

    def matches_position(self, x, y):
        if self.x <= x < self.x + self.width and self.y <= y < self.y + self.height:
            return True
        return False

    def add_window(self, window, x, y):
        if window in self.windows:
            del self.windows[self.windows.index(window)]
        pos = 0
        if self.windows:
            dy = max(y - self.y, 0)
            pos = int(dy / (self.height / (len(self.windows) + 1)))
            print(f"POS===>{pos}")
        self.windows.insert(pos, window)
        self._resize_windows()

    def _resize_windows(self):
        # Tile windows
        if not self.windows:
            return
        h = int(self.height / len(self.windows))
        for i, w in enumerate(self.windows):
            w.configure(
                x=self.x,
                y=self.y + i * h,
                width=self.width,
                height=h,
                stack_mode=X.Above,
            )

    def remove_window(self, window):
        if not window in self.windows:
            return
        del self.windows[self.windows.index(window)]
        self._resize_windows()


class Layout:
    def __init__(self):
        self.managers = []
        self.managers_per_window = {}

    def add_manager(self, manager):
        self.managers.append(manager)

    def get_manager(self, window):
        return self.managers_per_window.get(window)

    def add_window(self, window, x, y):
        raise NotImplementedError("Implement this")

    def remove_window(self, window):
        if not window.id in self.managers_per_window:
            return
        mgr = self.managers_per_window.pop(window.id)
        mgr.remove_window(window)

    def manager_for_position(self, x, y):
        for mgr in self.managers:
            if mgr.matches_position(x, y):
                return mgr
        print(
            f"Warning: no known manager for position {x},{y} ; returning the first one"
        )
        return self.managers[0]

    def move_window_to(self, window, x, y):
        old_mgr = self.managers_per_window.pop(window.id)
        new_mgr = self.manager_for_position(x, y)
        old_mgr.remove_window(window)
        new_mgr.add_window(window, x, y)
        self.managers_per_window[window.id] = new_mgr


class TwoColumnLayout(Layout):
    def __init__(self, root_window):
        super().__init__()
        root_geom = root_window.get_geometry()
        half_w = int(root_geom.width / 2)
        self.add_manager(WindowColumn(0, 0, half_w, root_geom.height))
        self.add_manager(WindowColumn(half_w, 0, half_w, root_geom.height))

    def add_window(self, window, x, y):
        mgr = self.manager_for_position(x, y)
        mgr.add_window(window, x, y)
        self.managers_per_window[window.id] = self.managers[-1]

    def manager_for_position(self, x, y):
        # If target is leftish, return left column, if rightish, right column
        mgr = super().manager_for_position(x, y)
        if mgr is None:
            if x <= 0:
                mgr = self.managers[0]
            else:
                mgr = self.managers[1]
        return mgr
