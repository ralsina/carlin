from Xlib import X


class Mouse:
    def __init__(self, layout):
        self.drag_window = None
        self.move_x = 0
        self.move_y = 0
        self.layout = layout

    def configure_mouse(self, window):
        window.grab_button(
            0,
            X.Mod1Mask,
            True,
            X.ButtonMotionMask | X.ButtonReleaseMask | X.ButtonPressMask,
            X.GrabModeAsync,
            X.GrabModeAsync,
            X.NONE,
            X.NONE,
            None,
        )

    def handle_mouse_event(self, event):
        print("====>", event.detail)
        if event.detail == 0 and event.type == X.MotionNotify:
            if self.drag_window is None:
                print("Starting move...")
                self.drag_window = event.window
                window_geometry = self.drag_window.get_geometry()
                self.move_x = window_geometry.x - event.root_x
                self.move_y = window_geometry.y - event.root_y
            else:
                print("Moving...")
                self.drag_window.configure(
                    x=self.move_x + event.root_x, y=self.move_y + event.root_y
                )

        elif event.type == X.ButtonRelease:
            # Let the layout manager handle this
            if self.drag_window:
                window_geometry = self.drag_window.get_geometry()
                self.layout.move_window_to(
                    event.window, window_geometry.x, window_geometry.y
                )
                self.drag_window = None

        elif event.detail == 2 and event.type == X.ButtonPress:
            event.window.set_input_focus(X.RevertToParent, X.CurrentTime)
            event.window.configure(stack_mode=X.Above)
