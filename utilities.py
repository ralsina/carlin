"""
Utilities
Set of basic utilities for HackWM
by Dylan Hamer
"""

import subprocess


def run_process(process):
    print("Running: " + str(process))
    try:
        subprocess.Popen(process)
    except Exception:
        print("An error occured while launching: " + process)
