"""
Wallpaper
Currently just calls Feh to provide a wallpaper but will soon be a standalone wallpaper manager
by Dylan Hamer
"""

from utilities import run_process


def set_wallpaper():
    run_process(["feh", "--bg-scale", "DefaultWallpaper.jpeg"])
