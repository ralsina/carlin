from Xlib import X

from mouse import Mouse
from colour import Colour


class Mapping:
    def __init__(self, display, layout, mouse):
        self.layout = layout
        self.display = display
        self.mouse_handler = mouse
        # TODO: initialize window_list with the list of windows
        self.window_list = []

    def handle_map_event(self, event):
        event.window.map()  # Map window
        self.window_list.append(event.window)  # Add window to list of open windows
        self.mouse_handler.configure_mouse(event.window)  # Configure mouse events
        self.layout.add_window(event.window, 0, 0)
        event.window.set_input_focus(X.RevertToParent, X.CurrentTime)  # Focus window
        event.window.configure(stack_mode=X.Above)  # Raise window to top

    def handle_unmap_event(self, event):
        event.window.unmap()
        self.layout.remove_window(event.window)

    def draw_borders(self):
        colour_handler = Colour(self.display)

        for window in self.window_list:
            window.configure(border_width=2)
            if self.display.get_input_focus().focus == window:
                border_colour = colour_handler.colours["white"]
            else:
                border_colour = colour_handler.colours["black"]
            border_colour = colour_handler.get_colour(border_colour)
            window.change_attributes(None, border_pixel=border_colour)
