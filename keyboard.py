"""
HackWM Keyboard Manager
Handles binding of keys and keypress events in HackWM
by Dylan Hamer
"""

from Xlib import X, XK
from utilities import run_process

MODIFIER = X.Mod1Mask  # ALT as modifier


class Keyboard:
    def __init__(self, display, root_window):
        self.display = display
        self.root_window = root_window
        self.configure_keys()

    def get_keycodes(self, key):
        """Return all possible keycodes for a certain key"""
        codes = set(code for code, index in self.display.keysym_to_keycodes(key))
        return codes

    def configure_keys(self):
        """Bind keys"""
        grabbed_keys = [
            [X.Mod1Mask, XK.XK_T],
            [X.NONE, X.Mod1Mask],
        ]
        for key_binding in grabbed_keys:  # For each key to grab,
            modifier = key_binding[0]
            key = key_binding[1]
            codes = self.get_keycodes(key)
            for code in codes:  # For each code
                self.root_window.grab_key(
                    code, modifier, 1, X.GrabModeAsync, X.GrabModeAsync
                )  # Receive events when the key is pressed

    def handle_key_event(self, event):
        """Handle key presses"""
        if event.detail in self.get_keycodes(XK.XK_T):
            run_process("/usr/bin/kitty")
