named_colours = {"black": "#000000", "white": "#ffffff"}


class Colour:
    def __init__(self, display):
        self.display = display
        self.colormap = self.display.screen().default_colormap
        self.colours = named_colours

    def get_colour(self, colour):
        return self.colormap.alloc_named_color(colour).pixel
